APP_NAME = app
DEBUG = 1
PLATFORM = linux

CC = clang++
CFLAGS = -std=c++17 -Wall

LINKER = clang++
LDFLAGS = -lvulkan

source_path = source/
source_files = \
	main.cpp \
	application.cpp \
	window.cpp \
	renderer.cpp \
	image_buffer.cpp \
	render_target.cpp \
	render_window.cpp

include_path = include/

bin_path = bin/

ifeq ($(PLATFORM),linux)
	source_files += \
		application_xcb.cpp \
		window_xcb.cpp

	LDFLAGS += -lxcb

else ifeq ($(PLATFORM),windows)
	source_files += \
		application_win32.cpp \
		window_win32.cpp

	LDFLAGS += -mwindows -municode

endif

ifdef DEBUG
	CFLAGS += -g -ggdb3
else
	CFLAGS += -O3 -DNDEBUG
endif

APP = $(APP_NAME:%=$(bin_path)%)

CFLAGS += -I$(include_path)

source = $(source_files:%=$(source_path)%)
object = $(source:.cpp=.o)

all: $(APP)

run: $(APP)
	@./$(APP)

$(APP): $(object)
	@mkdir -p $(bin_path)
	$(LINKER) -o $@ $(LDFLAGS) $^

%.o: %.cpp
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	rm -f $(APP) $(object)
