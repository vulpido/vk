#include "application.hpp"

#include <string>
#include <memory>
#include <iostream>

void Application::create_renderer() {
	renderer = std::make_shared<Renderer>(*this, main_window);
}

const std::string& Application::get_name() const {
	return name;
}

const VersionInfo& Application::get_version() const {
	return version;
}

std::shared_ptr<Window> Application::get_main_window() const {
	return main_window;
}

void Application::run() {
	main_window = std::make_shared<Window>(*this, name, 1600, 900);
	windows.push_back(main_window);

	create_renderer();

	while (not should_exit) {
		process_events();
	}
}
