#include "render_target.hpp"

#include <array>
#include <vulkan/vulkan.hpp>

#include "renderer.hpp"

RenderTarget::RenderTarget(
	const Renderer &r,
	vk::RenderPass render_pass,
	vk::ImageCreateInfo image_info,
	vk::MemoryPropertyFlags memory_properties,
	vk::ImageViewCreateInfo view_info
)
:	renderer(r)
,	image_buffer(r, image_info, memory_properties, view_info)
{
	create_framebuffer(render_pass);
}

RenderTarget::RenderTarget(
	const Renderer &r,
	vk::RenderPass render_pass,
	vk::Image image,
	uint32_t width,
	uint32_t height,
	vk::ImageViewCreateInfo view_info
)
:	renderer(r)
,	image_buffer(r, image, width, height, view_info)
{
	create_framebuffer(render_pass);
}

void RenderTarget::create_framebuffer(vk::RenderPass render_pass) {
	if (framebuffer) {
		return;
	}
	
	std::array<vk::ImageView, 1> attachments = {
		image_buffer.get_image_view()
	};
	
	vk::FramebufferCreateInfo info;
	info.renderPass      = render_pass;
	info.attachmentCount = static_cast<uint32_t>(attachments.size());
	info.pAttachments    = attachments.data();
	info.width           = image_buffer.get_extent().width;
	info.height          = image_buffer.get_extent().height;
	info.layers          = 1;
	
	vk::Device device = renderer.get_device();
	framebuffer = device.createFramebufferUnique(info);
	
	if (not framebuffer) {
		throw std::runtime_error("RenderTarget::create_framebuffer - failed");
	}
}

const ImageBuffer& RenderTarget::get_image_buffer() const {
	return image_buffer;
}

vk::Image RenderTarget::get_image() const {
	return image_buffer.get_image();
}

vk::ImageView RenderTarget::get_image_view() const {
	return image_buffer.get_image_view();
}

vk::Extent2D RenderTarget::get_extent() const {
	return image_buffer.get_extent();
}

vk::Framebuffer RenderTarget::get_framebuffer() const {
	return *framebuffer;
}
