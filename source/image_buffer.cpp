#include "image_buffer.hpp"

#include <vulkan/vulkan.hpp>

#include "renderer.hpp"

ImageBuffer::ImageBuffer(
	const Renderer &r,
	vk::ImageCreateInfo image_info,
	vk::MemoryPropertyFlags memory_properties,
	vk::ImageViewCreateInfo view_info
) : renderer(r)
{
	vk::Device device = renderer.get_device();
	
	image = device.createImage(image_info);
	
	if (not image) {
		throw std::runtime_error("ImageBuffer::ImageBuffer - failed to create image");
	}
	
	view_info.image = image;
	
 	auto memory_requirements = device.getImageMemoryRequirements(image);
	uint32_t memory_index = renderer.find_memory_type(memory_requirements.memoryTypeBits, memory_properties);
	
	vk::MemoryAllocateInfo memory_info;
	memory_info.allocationSize = memory_requirements.size;
	memory_info.memoryTypeIndex = memory_index;
	
	memory = device.allocateMemory(memory_info);
	if (not memory) {
		throw std::runtime_error("ImageBuffer::ImageBuffer - failed to allocate image memory");
	}
	
	device.bindImageMemory(image, memory, 0);
	
	image_view = device.createImageViewUnique(view_info);
	if (not image_view) {
		throw std::runtime_error("ImageBuffer::ImageBuffer - failed to create image view");
	}
	
	extent.width = image_info.extent.width;
	extent.height = image_info.extent.height;
	
	delete_image = true;
}

ImageBuffer::ImageBuffer(
	const Renderer &r,
	vk::Image image,
	uint32_t width,
	uint32_t height,
	vk::ImageViewCreateInfo view_info
) : renderer(r)
{
	view_info.image = image;
	
	vk::Device device = renderer.get_device();
	image_view = device.createImageViewUnique(view_info);
	
	if (not image_view) {
		throw std::runtime_error("ImageBuffer::ImageBuffer - failed to create image view");
	}
	
	extent.width = width;
	extent.height = height;
	
	delete_image = false;
}

ImageBuffer::~ImageBuffer() {
	if (delete_image) {
		vk::Device device = renderer.get_device();
		
		device.destroyImage(image);
		device.freeMemory(memory);
	}
}

vk::Image ImageBuffer::get_image() const {
	return image;
}

vk::ImageView ImageBuffer::get_image_view() const {
	return *image_view;
}

vk::Extent2D ImageBuffer::get_extent() const {
	return extent;
}
