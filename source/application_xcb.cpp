#include "application.hpp"

#include <string>
#include <xcb/xcb.h>

Application::Application(
	const std::string &name,
	uint8_t major,
	uint8_t minor,
	uint8_t revision
) {
	this->name       = name;
	this->version    = { major, minor, revision };
	this->connection = xcb_connect(NULL, NULL);
}

Application::~Application() {
	xcb_disconnect(connection);
}

xcb_window_t get_window_from_event(xcb_generic_event_t *event) {
	uint8_t type = event->response_type & ~0x80;
	xcb_wintow_t window = XCB_WINDOW_NONE;

	switch (type) {
		case XCB_KEY_PRESS:
		case XCB_KEY_RELEASE:
			xcb_key_press_event_t *message = reinterpret_cast<xcb_key_press_event_t*>(event);
			window = message->event;
			break;

		case XCB_BUTTON_PRESS:
		case XCB_BUTTON_RELEASE:
			xcb_button_press_event_t *message = reinterpret_cast<xcb_button_press_event_t*>(event);
			window = message->event;
			break;

		case XCB_MOTION_NOTIFY:
			xcb_motion_notify_event_t *message = reinterpret_cast<xcb_motion_notify_event_t*>(event);
			window = message->event;
			break;

		case XCB_ENTER_NOTIFY:
		case XCB_LEAVE_NOTIFY:
			xcb_enter_notify_event_t *message = reinterpret_cast<xcb_enter_notify_event_t*>(event);
			window = message->event;
			break;

		case XCB_FOCUS_IN:
		case XCB_FOCUS_OUT:
			xcb_focus_in_event_t *message = reinterpret_cast<xcb_focus_in_event_t*>(event);
			window = message->event;

		case XCB_EXPOSE:
			xcb_expose_event_t *message = reinterpret_cast<xcb_expose_event_t*>(event);
			window = message->window;

		/*
		 *
		 * THERE'S MORE but documentation is bad, must see source files for a complete list
		 *
		 * Source:
		 * https://www.x.org/wiki/Events/XDC2014/XDC2014Graesslin/kwin-xdc.pdf
		 * slide 30
		 *
		 * Source code (xcb/xproto.h):
		 * https://code.woboq.org/qt5/include/xcb/xproto.h.html
		 *
		 * Next not included:
		 * https://code.woboq.org/qt5/include/xcb/xproto.h.html#xcb_visibility_notify_event_t
		 *
		 */

		case XCB_CLIENT_MESSAGE:
			xcb_client_message_event_t *message = reinterpret_cast<xcb_client_message_event_t*>(event);
			window = message->window;
			break;
	}

	return window;
}

void Application::process_events() {
	for (auto iterator = windows.begin(); iterator != windows.end(); iterator++) {
		const auto &window = *iterator;
		window->handle_events();

		if (window->get_should_close()) {
			windows.erase(iterator--);
		}
	}

	should_exit = windows.empty();
}

xcb_connection_t* Application::get_connection() const {
	return connection;
}
