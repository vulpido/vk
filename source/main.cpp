#include <memory>
#include <string>
#include <iostream>

#include "application.hpp"

#if defined(__WIN32)

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow) {
	std::unique_ptr<Application> app = std::make_unique<Application>(hInstance, "Vulkan Test Application", 0, 1, 0);

	try {
		app->run();
	} catch (const std::exception &e) {
		std::cerr << e.what() << std::endl;
		return 1;
	}

	return 0;
}

#else

int main() {
	std::unique_ptr<Application> app = std::make_unique<Application>("Vulkan Test Application", 0, 1, 0);

	try {
		app->run();
	} catch (const std::exception &e) {
		std::cerr << e.what() << std::endl;
		return 1;
	}

	return 0;
}

#endif
