#include "render_window.hpp"

#include "renderer.hpp"
#include "window.hpp"

RenderWindow::RenderWindow(
	const Renderer &r,
	const Window &w
)
: renderer(r)
, window(w)
{
	create_surface();
}

void RenderWindow::create_surface() {
	vk::Instance instance = renderer.get_instance();
	
#if defined(__linux__)
	vk::XcbSurfaceCreateInfoKHR info;
	info.connection = window.get_connection();
	info.window = window.get_handle();
	
	surface = instance.createXcbSurfaceKHRUnique(info);

#elif defined(__WIN32)
	vk::Win32SurfaceCreateInfoKHR info;
	info.hinstance = window.get_hinstance();
	info.hwnd = window.get_handle();
	
	surface = instance.createWin32SurfaceKHRUnique(info);
	
#endif
	
	if (not surface) {
		throw std::runtime_error("RenderWindow::create_surface - failed");
	}
}

vk::Extent2D choose_swapchain_extent(
	vk::SurfaceCapabilitiesKHR capabilities,
	uint32_t width,
	uint32_t height
) {
	if (capabilities.currentExtent.width != UINT32_MAX) {
		return capabilities.currentExtent;
	}
	
	vk::Extent2D extent = { width, height };
	
	extent.width  = std::clamp(extent.width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width);
	extent.height = std::clamp(extent.height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height);
	
	return extent;
}

void RenderWindow::create_swapchain() {
	SwapchainSupport support(renderer.get_physical_device(), *surface);
	
	vk::PresentModeKHR present_mode = renderer.choose_swapchain_present_mode(support.present_modes);
	vk::SurfaceFormatKHR surface_format = renderer.choose_swapchain_format(support.formats);
	extent = choose_swapchain_extent(support.capabilities, window.get_width(), window.get_height());
	format = surface_format.format;
	
	uint32_t image_count = std::max(support.capabilities.minImageCount + 1, support.capabilities.maxImageCount);
	
	vk::SwapchainCreateInfoKHR info;
	info.surface          = *surface;
	info.minImageCount    = image_count;
	info.imageFormat      = format;
	info.imageColorSpace  = surface_format.colorSpace;
	info.imageExtent      = extent;
	info.imageArrayLayers = 1;
	info.imageUsage       = vk::ImageUsageFlagBits::eColorAttachment;
	
	std::array<uint32_t, 2> queue_family_indices = { graphics_queue_index, present_queue_index };
	
	if (graphics_queue_index != present_queue_index) {
		info.imageSharingMode      = vk::SharingMode::eConcurrent;
		info.queueFamilyIndexCount = static_cast<uint32_t>(queue_family_indices.size());
		info.pQueueFamilyIndices   = queue_family_indices.data();
	} else {
		info.imageSharingMode      = vk::SharingMode::eExclusive;
		info.queueFamilyIndexCount = 0;
		info.pQueueFamilyIndices   = nullptr;
	}
	
	info.preTransform   = support.capabilities.currentTransform;
	info.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;
	info.presentMode    = present_mode;
	info.oldSwapchain   = nullptr;
	
	vk::Device device = renderer.get_device();
	swapchain = device.createSwapchainKHRUnique(info);
	
	if (not swapchain) {
		throw std::runtime_error("Renderer::create_swapchain - failed to create swapchain");
	}
	
	create_render_pass();
	create_render_targets();
}

void RenderWindow::create_render_pass() {
	vk::AttachmentDescription color_attachment;
	color_attachment.format  = format;
	color_attachment.samples = vk::SampleCount::e1Bit;
	color_attachment.loadOp  = vk::AttachmentLoadOp::eClear;
	color_attachment.storeOp = vk::AttachmentStoreOp::eStore;
	color_attachment.stencilLoadOp  = vk::AttachmentLoadOp::eDontCare;
	color_attachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
	color_attachment.initialLayout  = vk::ImageLayout::eUndefined;
	color_attachment.finalLayout    = vk::ImageLayout::ePresentSrcKHR;
	
	vk::AttachmentReference color_reference;
	color_reference.attachment = 0;
	color_reference.layout = vk::ImageLayout::eColorAttachmentOptimal;
	
	vk::SubpassDescription subpass;
	subpass.pipelineBindPoint    = vk::PipelineBindPoint::eGraphics;
	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments    = &color_reference;
	
	std::vector<vk::AttachmentDescription> attachments = { color_attachment };
	
	vk::RenderPassCreateInfo info;
	info.attachmentCount = static_cast<uint32_t>(attachments.size());
	info.pAttachments    = attachments.data();
	info.subpassCount    = 1;
	info.pSubpasses      = &subpass;
	
	vk::Device device = renderer.get_device();
	render_pass = device.createRenderPassUnique(info);
}

void RenderWindow::create_render_targets() {
	vk::Device device = renderer.get_device();
	std::vector<vk::Image> swapchain_images = device.getSwapchainImagesKHR(*swapchain);
	
	for (const auto &image : swapchain_images) {
		vk::ImageViewCreateInfo info;
		info.image    = image;
		info.viewType = vk::ImageViewType::e2D;
		info.format   = format;
		info.components.r = vk::ComponentSwizzle::eIdentity;
		info.components.g = vk::ComponentSwizzle::eIdentity;
		info.components.b = vk::ComponentSwizzle::eIdentity;
		info.components.a = vk::ComponentSwizzle::eIdentity;
		info.subresourceRange.aspectMask     = vk::ImageAspectFlagBits::eColor;
		info.subresourceRange.baseMipLevel   = 0;
		info.subresourceRange.levelCount     = 1;
		info.subresourceRange.baseArrayLayer = 0;
		info.subresourceRange.layerCount     = 1;
		
		render_targets.push_back(
			RenderTarget(
				renderer,
				render_pass,
				image,
				extent.width,
				extent.height,
				info
			)
		);
	}
}

vk::SurfaceKHR RenderWindow::get_surface() const {
	return *surface;
}

vk::Format RenderWindow::get_format() const {
	return format;
}

vk::Extent2D RenderWindow::get_extent() const {
	return extent;
}

vk::SwapchainKHR RenderWindow::get_swapchain() const {
	return *swapchain;
}

const std::vector<RenderTarget>& RenderWindow::get_render_targets() const {
	return render_targets;
}
