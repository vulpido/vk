#include "window.hpp"

void Window::set_visible(bool visible) {
	if (visible) {
		show();
	} else {
		hide();
	}
}

bool Window::get_should_close() const {
	return should_close;
}
