#include "window.hpp"

#include <string>
#include <xcb/xcb.h>
#include <iostream>

#include "application.hpp"

Window::Window(
	const Application &app,
	const std::string &title,
	uint32_t width,
	uint32_t height
) : application(app)
{
	connection = application.get_connection();
	
	const xcb_setup_t *setup = xcb_get_setup(connection);
	xcb_screen_iterator_t iterator = xcb_setup_roots_iterator(setup);
	xcb_screen_t *screen = iterator.data;
	
	// make the window centered
	//uint32_t x = (screen->width_in_pixels  - width)  / 2;
	uint32_t x = screen->width_in_pixels / 2 - width / 2;
	uint32_t y = (screen->height_in_pixels - height) / 2;
	
	uint32_t mask = XCB_CW_EVENT_MASK;
	uint32_t values[1] = { XCB_CLIENT_MESSAGE };
	
	handle = xcb_generate_id(connection);
	xcb_create_window(
		connection,                     // connection
		XCB_COPY_FROM_PARENT,           // depth
		handle,                         // window id
		screen->root,                   // parent
		x,                              // x
		y,                              // y
		width,                          // width
		height,                         // height
		10,                             // border width
		XCB_WINDOW_CLASS_INPUT_OUTPUT,  // window class
		screen->root_visual,            // visual
		mask,                           // mask
		values                          // mask values
	);
	
	set_title(title);
	
	xcb_intern_atom_cookie_t cookie_protocols = xcb_intern_atom(connection, 1, 12, "WM_PROTOCOLS");
	xcb_intern_atom_reply_t *reply_protocols = xcb_intern_atom_reply(connection, cookie_protocols, 0);
	
	xcb_intern_atom_cookie_t cookie_delete = xcb_intern_atom(connection, 0, 16, "WM_DELETE_WINDOW");
	reply_window_delete = xcb_intern_atom_reply(connection, cookie_delete, 0);
	
	xcb_change_property(
		connection,
		XCB_PROP_MODE_REPLACE,
		handle,
		reply_protocols->atom,
		XCB_ATOM_ATOM,
		32, 1,
		&reply_window_delete->atom
	);
	
	show();
	
	xcb_flush(connection);
	free(reply_protocols);
}

Window::~Window() {
	free(reply_window_delete);
	
	xcb_destroy_window(connection, handle);
	xcb_flush(connection);
}

void Window::show() {
	xcb_map_window(connection, handle);
	xcb_flush(connection);
}

void Window::hide() {
	xcb_unmap_window(connection, handle);
	xcb_flush(connection);
}

bool Window::get_visible() const {
	bool visible = false;
	
	xcb_get_window_attributes_cookie_t cookie = xcb_get_window_attributes(connection, handle);
	xcb_get_window_attributes_reply_t *reply = xcb_get_window_attributes_reply(connection, cookie, NULL);
	
	if (reply) {
		visible = (reply->map_state != XCB_MAP_STATE_UNMAPPED);
		free(reply);
	}
	
	return visible;
}

uint32_t Window::get_width() const {
	uint32_t width = 0;
	
	xcb_get_geometry_cookie_t cookie = xcb_get_geometry(connection, handle);
	xcb_get_geometry_reply_t *reply = xcb_get_geometry_reply(connection, cookie, NULL);
	
	if (reply) {
		width = reply->width;
		free(reply);
	}
	
	return width;
}

uint32_t Window::get_height() const {
	uint32_t height = 0;
	
	xcb_get_geometry_cookie_t cookie = xcb_get_geometry(connection, handle);
	xcb_get_geometry_reply_t *reply = xcb_get_geometry_reply(connection, cookie, NULL);
	
	if (reply) {
		height = reply->height;
		free(reply);
	}
	
	return height;
}

std::string Window::get_title() const {
	const uint32_t max_length = 100;
	
	std::string title = "";
	
	xcb_get_property_cookie_t cookie = xcb_get_property(
		connection,
		0,
		handle,
		XCB_ATOM_WM_NAME,
		XCB_ATOM_STRING,
		0,
		max_length
	);
	
	xcb_get_property_reply_t *reply;
	if ((reply = xcb_get_property_reply(connection, cookie, NULL))) {
		int length = xcb_get_property_value_length(reply);
		
		if (length <= 0) {
			std::cerr << "XCB_ATOM_WM_NAME has length zero" << std::endl;
		} else {
			title = (char*)xcb_get_property_value(reply);
		}
		
		free(reply);
	}
	
	return title;
}

void Window::set_title(const std::string &title) {
	xcb_change_property(
		connection,
		XCB_PROP_MODE_REPLACE,
		handle,
		XCB_ATOM_WM_NAME,
		XCB_ATOM_STRING,
		8,
		title.length(),
		title.c_str()
	);
	
	xcb_flush(connection);
}

void Window::handle_events() {
	xcb_generic_event_t *event;
	
	while ((event = xcb_poll_for_event(connection))) {
		uint8_t type = event->response_type & ~0x80;
		
		switch (type) {
			case XCB_CLIENT_MESSAGE: {
				xcb_client_message_event_t *message = reinterpret_cast<xcb_client_message_event_t*>(event);
				
				//std::cout << message->window << std::endl;
				//std::cout << handle << std::endl;
				
				if (message->window == handle) {
					if (message->data.data32[0] == reply_window_delete->atom) {
						should_close = true;
					}
				}
				
				// TODO: is it's not the same window, the event is consumed but not handled
				
			} break;
		}
	}
}

xcb_connection_t* Window::get_connection() const {
	return connection;
}

xcb_window_t Window::get_handle() const {
	return handle;
}
