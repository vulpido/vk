#include "renderer.hpp"

#include <string>
#include <iostream>
#include <set>
#include <array>
#include <vulkan/vulkan.h>
#include <vulkan/vulkan.hpp>

#include "application.hpp"
#include "window.hpp"

static VKAPI_ATTR VkBool32 VKAPI_CALL debug_callback(
	VkDebugUtilsMessageSeverityFlagBitsEXT severity,
	VkDebugUtilsMessageTypeFlagsEXT type,
	const VkDebugUtilsMessengerCallbackDataEXT *callback_data,
	void *user_data
) {
	std::string level = vk::to_string(vk::DebugUtilsMessageSeverityFlagBitsEXT(severity));
	std::cerr << "[vk] " << level << " - " << callback_data->pMessage << std::endl;
	
	return VK_FALSE;
}

Renderer::Renderer(const Application &app, std::shared_ptr<Window> window) : application(app) {
	const VersionInfo &version = application.get_version();
	create_instance(application.get_name(), version.major, version.minor, version.revision);
	
	if (use_validation_layers()) {
		setup_debug_messenger();
	}
	
	create_surface(window);
	pick_physical_device();
	create_logical_device();
}

Renderer::~Renderer() {}

void fill_debug_info(vk::DebugUtilsMessengerCreateInfoEXT &info) {
	info = vk::DebugUtilsMessengerCreateInfoEXT{};
	
	info.messageSeverity =
		//vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose |
		//vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo |
		vk::DebugUtilsMessageSeverityFlagBitsEXT::eError |
		vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning;
	
	info.messageType =
		vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral |
		vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation |
		vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance;
	
	info.pfnUserCallback = debug_callback;
	info.pUserData = nullptr;
}

bool Renderer::use_validation_layers() const {
#ifdef NDEBUG
	return false;
#else
	return true;
#endif
}

std::vector<const char*> Renderer::get_validation_layers() const {
	std::vector<const char*> layers = { "VK_LAYER_KHRONOS_validation" };
	return layers;
}

std::vector<const char*> Renderer::get_instance_extensions() const {
	std::vector<const char*> extensions = {
		"VK_KHR_surface",
		
		#ifdef __linux__
			"VK_KHR_xcb_surface"
			
		#elif _WIN32
			"VK_KHR_win32_surface"
			
		#endif
	};
	
	if (use_validation_layers()) {
		extensions.push_back("VK_EXT_debug_utils");
	}
	
	return extensions;
}

std::vector<const char*> Renderer::get_device_extensions() const {
	return { "VK_KHR_swapchain" };
}

void Renderer::create_instance(const std::string &name, uint32_t major, uint32_t minor, uint32_t revision) {
	vk::ApplicationInfo app_info;
	app_info.pApplicationName = name.c_str();
	app_info.applicationVersion = VK_MAKE_VERSION(major, minor, revision);
	app_info.pEngineName = "";
	app_info.engineVersion = VK_MAKE_VERSION(1, 0, 0);
	app_info.apiVersion = VK_API_VERSION_1_0;
	
	std::vector<const char*> extensions = get_instance_extensions();
	
	vk::InstanceCreateInfo info;
	info.pApplicationInfo = &app_info;
	info.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
	info.ppEnabledExtensionNames = extensions.data();
	
	std::vector<const char*> layers;
	if (use_validation_layers()) {
		layers = get_validation_layers();
		
		info.enabledLayerCount = static_cast<uint32_t>(layers.size());
		info.ppEnabledLayerNames = layers.data();
		
		vk::DebugUtilsMessengerCreateInfoEXT messenger_info;
		fill_debug_info(messenger_info);
		
		info.pNext = &messenger_info;
	} else {
		info.enabledLayerCount = 0;
		info.ppEnabledLayerNames = nullptr;
		info.pNext = nullptr;
	}
	
	instance = vk::createInstanceUnique(info);
	
	if (not instance) {
		throw std::runtime_error("Renderer::create_instance - Failed to create vulkan instance");
	}
	
	std::cout << "Renderer::create_instance - Instance created OK" << std::endl;
}

void Renderer::setup_debug_messenger() {
	dynamic_loader = vk::DispatchLoaderDynamic(*instance, vkGetInstanceProcAddr);
	
	vk::DebugUtilsMessengerCreateInfoEXT messenger_info;
	fill_debug_info(messenger_info);
	
	messenger = instance->createDebugUtilsMessengerEXTUnique(messenger_info, nullptr, dynamic_loader);
	
	if (not messenger) {
		std::cerr << "Renderer::Renderer - Failed to create vulkan validation layer messenger" << std::endl;
	}
}

void Renderer::create_surface(std::shared_ptr<Window> window) {
#ifdef __linux__
// 	vk::XcbSurfaceCreateInfoKHR info;
// 	info.connection = application.get_connection();
// 	info.window = window->get_handle();
// 	
// 	surface = instance->createXcbSurfaceKHRUnique(info);
	
#elif _WIN32
// 	vk::Win32SurfaceCreateInfoKHR info;
// 	info.hinstance = hinstance;
// 	info.hwnd = hwnd;
// 	
// 	surface instance->createWin32SurfaceKHRUnique(info);
#endif
	
	if (not surface) {
		throw std::runtime_error("Renderer::create_surface - Failed to create surface");
	}
	
	std::cout << "Renderer::create_surface - Surface created OK" << std::endl;
}

struct QueueFamilyIndices {
	std::optional<uint32_t> graphics;
	std::optional<uint32_t> present;
	
	inline bool is_complete() const {
		return graphics.has_value() and present.has_value();
	}
};

QueueFamilyIndices find_queue_family_indices(vk::PhysicalDevice device, vk::SurfaceKHR surface) {
	QueueFamilyIndices indices;
	
	std::vector<vk::QueueFamilyProperties> families = device.getQueueFamilyProperties();
	
	for (uint32_t index = 0; index < families.size(); index++) {
		const auto &family = families[index];
		
		if (family.queueFlags & vk::QueueFlagBits::eGraphics) {
			indices.graphics = index;
		}
		
		bool present_support = device.getSurfaceSupportKHR(index, surface);
		if (present_support) {
			indices.present = index;
		}
		
		if (indices.is_complete()) {
			break;
		}
	}
	
	return indices;
}

SwapchainSupport::SwapchainSupport(vk::PhysicalDevice device, vk::SurfaceKHR surface) {
	capabilities = device.getSurfaceCapabilitiesKHR(surface);
	formats = device.getSurfaceFormatsKHR(surface);
	present_modes = device.getSurfacePresentModesKHR(surface);
}

vk::SurfaceFormatKHR Renderer::choose_swapchain_format(const std::vector<vk::SurfaceFormatKHR> &formats) const {
	for (const auto &format : formats) {
		if (format.format == vk::Format::eB8G8R8A8Srgb and format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear
		) {
			return format;
		}
	}
	
	return formats[0];
}

vk::PresentModeKHR Renderer::choose_swapchain_present_mode(const std::vector<vk::PresentModeKHR> &present_modes) const {
	for (const auto &present_mode : present_modes) {
		if (present_mode == vk::PresentModeKHR::eMailbox) {
			return present_mode;
		}
	}
	
	return vk::PresentModeKHR::eFifo;
}

bool Renderer::is_device_suitable(vk::PhysicalDevice device) {
	bool suitable = false;
	
	vk::PhysicalDeviceProperties properties = device.getProperties();
	suitable = (properties.deviceType == vk::PhysicalDeviceType::eDiscreteGpu);
	
	QueueFamilyIndices queue_family_indices = find_queue_family_indices(device, *surface);
	suitable = suitable and queue_family_indices.is_complete();
	
	SwapchainSupport swapchain_support(device, *surface);
	suitable = suitable and not swapchain_support.formats.empty();
	suitable = suitable and not swapchain_support.present_modes.empty();
	
	return suitable;
}

void Renderer::pick_physical_device() {
	std::vector<vk::PhysicalDevice> devices = instance->enumeratePhysicalDevices();
	
	if (devices.empty()) {
		throw std::runtime_error("Renderer::Renderer - No compatible device found");
	}
	
	for (const auto &device : devices) {
		if (is_device_suitable(device)) {
			physical_device = device;
			
			QueueFamilyIndices queue_family_indices = find_queue_family_indices(device, *surface);
			if (queue_family_indices.is_complete()) {
				graphics_queue_index = queue_family_indices.graphics.value();
				present_queue_index = queue_family_indices.present.value();
			} else {
				throw std::runtime_error("Renderer::pick_physical_device - no graphics or present queue");
			}
			
			break;
		}
	}
	
	//  If there is no discrete GPU, pick first device available
	if (not physical_device) {
		physical_device = devices[0];
		std::cerr << "Renderer::pick_physical_device - No discrete GPU found" << std::endl;
	}
	
	vk::PhysicalDeviceProperties properties = physical_device.getProperties();
	std::cout << "Renderer::pick_physical_device - Selected device with name: " << properties.deviceName << std::endl;
}

void Renderer::create_logical_device() {
	float queue_priority = 1.0f;
	
	std::vector<vk::DeviceQueueCreateInfo> queue_infos;
	std::set<uint32_t> queue_family_indices = { graphics_queue_index, present_queue_index };
	
	for (uint32_t family_index : queue_family_indices) {
		vk::DeviceQueueCreateInfo queue_info;
		queue_info.queueFamilyIndex = family_index;
		queue_info.queueCount = 1;
		queue_info.pQueuePriorities = &queue_priority;
		
		queue_infos.push_back(queue_info);
	}
	
	vk::PhysicalDeviceFeatures device_features;
	
	std::vector<const char*> extensions = get_device_extensions();
	
	vk::DeviceCreateInfo info;
	info.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
	info.ppEnabledExtensionNames = extensions.data();
	info.queueCreateInfoCount = static_cast<uint32_t>(queue_infos.size());
	info.pQueueCreateInfos = queue_infos.data();
	info.pEnabledFeatures = &device_features;
	
	if (use_validation_layers()) {
		std::vector<const char*> layers = get_validation_layers();
		
		info.enabledLayerCount = static_cast<uint32_t>(layers.size());
		info.ppEnabledLayerNames = layers.data();
	} else {
		info.enabledLayerCount = 0;
		info.ppEnabledLayerNames = nullptr;
	}
	
	device = physical_device.createDeviceUnique(info);
	
	if (not device) {
		throw std::runtime_error("Renderer::create_logical_device - failed to create device");
	}
	
	graphics_queue = device->getQueue(graphics_queue_index, 0);
	present_queue = device->getQueue(present_queue_index, 0);
	
	std::cout << "Renderer::create_logical_device - Device created OK" << std::endl;
}

uint32_t Renderer::find_memory_type(uint32_t type_filter, vk::MemoryPropertyFlags flags) const {
	if (not physical_device) {
		std::cerr << "Renderer::find_memory_type - physical device is not set" << std::endl;
		return 0;
	}
	
	uint32_t memory_index = 0;
	
	vk::PhysicalDeviceMemoryProperties properties = physical_device.getMemoryProperties();
	
	for (uint32_t i = 0; i < properties.memoryTypeCount; i++) {
		if ((type_filter & (1 << i)) and ((properties.memoryTypes[i].propertyFlags & flags) == flags)) {
			memory_index = i;
			break;
		}
	}
	
	return memory_index;
}

vk::Instance Renderer::get_instance() const {
	return *instance;
}

vk::PhysicalDevice Renderer::get_physical_device() const {
	return physical_device;
}

vk::Device Renderer::get_device() const {
	return *device;
}
