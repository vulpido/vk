#include "application.hpp"

#include <string>
#include <windows.h>

Application(
	const std::string &name,
	uint8_t major,
	uint8_t minor,
	uint8_t revision
) {
	this->name      = name;
	this->version   = { major, minor, revision };
	this->hInstance = GetModuleHandle(NULL);
}

Application::~Application() {}

void Application::process_events() {
	MSG msg = {};
	should_exit = (GetMessage(&msg, NULL, 0, 0) <= 0);

	if (not should_exit) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

HINSTANCE Application::get_hinstance() const {
	return hInstance;
}
