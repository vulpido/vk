#pragma once

#include <vulkan/vulkan.hpp>
#include "image_buffer.hpp"

class Renderer;

class RenderTarget {
private:
	const Renderer& renderer;
	
	ImageBuffer image_buffer;
	vk::UniqueFramebuffer framebuffer;
	
	void create_framebuffer(vk::RenderPass);
	
public:
	RenderTarget(
		const Renderer&,
		vk::RenderPass,
		vk::ImageCreateInfo,
		vk::MemoryPropertyFlags,
		vk::ImageViewCreateInfo
	);
	
	RenderTarget(
		const Renderer&,
		vk::RenderPass,
		vk::Image,
		uint32_t width,
		uint32_t height,
		vk::ImageViewCreateInfo
	);
	
	~RenderTarget();
	
	const ImageBuffer& get_image_buffer() const;
	vk::Image get_image() const;
	vk::ImageView get_image_view() const;
	vk::Extent2D get_extent() const;
	
	vk::Framebuffer get_framebuffer() const;
};

// https://vulkan-tutorial.com/Drawing_a_triangle/Graphics_pipeline_basics/Render_passes
// https://vulkan-tutorial.com/Drawing_a_triangle/Drawing/Framebuffers
