#pragma once

#include <string>
#include <vector>
#include <memory>

#if defined(__linux__)
#include <xcb/xcb.h>

#elif defined(__WIN32)
#include <windows.h>

#endif

#include "window.hpp"
//#include "renderer.hpp"

class Renderer;

struct VersionInfo {
	uint8_t major;
	uint8_t minor;
	uint8_t revision;
};

class Application {
private:
#if defined(__linux__)
	xcb_connection_t *connection = nullptr;

#elif defined(__WIN32)
	HINSTANCE hInstance;

#endif

protected:
	std::string name;
	VersionInfo version;

	std::shared_ptr<Window> main_window;
	std::vector<std::shared_ptr<Window>> windows;

	std::shared_ptr<Renderer> renderer;

	bool should_exit = false;

	void create_renderer();
	void process_events();

public:
	Application(
		const std::string &name,
		uint8_t major,
		uint8_t minor,
		uint8_t revision
	);

	virtual ~Application();

	const std::string& get_name() const;
	const VersionInfo& get_version() const;
	std::shared_ptr<Window> get_main_window() const;

	void run();

#if defined(__linux__)
	xcb_connection_t* get_connection() const;

#elif defined(__WIN32)
	HINSTANCE get_hinstance() const;

#endif
};
