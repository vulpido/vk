#pragma once

#if defined(__linux__)
#define VK_USE_PLATFORM_XCB_KHR
#endif

#include <vector>
#include <vulkan/vulkan.hpp>

#include "render_target.hpp"

class Renderer;
class Window;

class RenderWindow {
private:
	const Renderer &renderer;
	const Window &window;
	
	vk::UniqueSurfaceKHR surface;
	
	vk::Format format;
	vk::Extent2D extent;
	vk::UniqueSwapchainKHR swapchain;
	vk::RenderPass render_pass;
	std::vector<RenderTarget> render_targets;
	
	void create_surface();
	void create_render_pass();
	void create_render_targets();
	
public:
	RenderWindow(const Renderer&, const Window&);
	
	void create_swapchain();
	
	vk::SurfaceKHR get_surface() const;
	vk::Format get_format() const;
	vk::Extent2D get_extent() const;
	vk::SwapchainKHR get_swapchain() const;
	const std::vector<RenderTarget>& get_render_targets() const;
};
