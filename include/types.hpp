#pragma once

struct Point {
	uint32_t x;
	uint32_t y;
};

struct Size {
	uint32_t width;
	uint32_t height;
};

struct Rectangle {
	Point position;
	Size size;
}
