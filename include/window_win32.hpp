#pragma once

#include <windows.h>

/* Guide:
 *
 * https://docs.microsoft.com/en-us/windows/win32/learnwin32/managing-application-state-#an-object-oriented-approach
 *
 */

template<class DerivedWindow>
class BaseWindow {
private:
	static LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
		DerivedWindow *instance = NULL;

		if (uMsg == WN_NCCREATE) {
			CREATESTRUCT *pCreate = (CREATESTRUCT*)lParam;
			instance = (DerivedWindow*)pCreate->lpCreateParams;

			SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)instance);
			instance->handle = hwnd;
		} else {
			instance = (DerivedWindow*)GetWindowLongPtr(hwnd, GWLP_USERDATA);
		}

		if (instance) {
			return instance->handle_message(uMsg, wParam, lParam);
		} else {
			return DefWindowProc(hwnd, uMsg, wParam, lParam);
		}
	}

protected:
	HWND handle;

	virtual PCWSTR get_class_name() const = 0;

	bool create(
		PCWSTR window_name,
		DWORD window_style,
		DWORD window_extended_style = 0,
		int x       = CW_USEDEFAULT,
		int y       = CW_USEDEFAULT,
		int width   = CW_USEDEFAULT,
		int height  = CW_USEDEFAULT,
		HWND parent = 0,
		HMENU menu  = 0
	) {
		WNDCLASS window_class      = {0};
		window_class.lpfnWndProc   = DerivedWindow::WindowProc;
		window_class.hInstance     = GetModuleHandle(NULL);
		window_class.lpszClassName = get_class_name();

		RegisterClass(&window_class);

		handle = CreateWindowEx(
			window_extended_style,
			get_class_name(),
			window_name,
			window_style,
			x,
			y,
			width,
			height,
			parent,
			menu,
			GetModuleHandle(NULL),
			this
		);

		return (handle != NULL);
	}

public:
	BaseWindow();

	virtual LRESULT handle_message(UINT msg, WPARAM wParam, LPARAM lParam) = 0;

	HWND get_handle() const {
		return handle;
	}
}
