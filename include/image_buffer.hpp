#pragma once

#include <vulkan/vulkan.hpp>

class Renderer;

class ImageBuffer {
private:
	const Renderer &renderer;
	
	bool delete_image;
	
	vk::Image image;
	vk::DeviceMemory memory;
	vk::UniqueImageView image_view;
	vk::Extent2D extent;
	
public:
	ImageBuffer(
		const Renderer&,
		vk::ImageCreateInfo,
		vk::MemoryPropertyFlags,
		vk::ImageViewCreateInfo
	);
	
	ImageBuffer(
		const Renderer&,
		vk::Image,
		uint32_t width,
		uint32_t height,
		vk::ImageViewCreateInfo
	);
	
	~ImageBuffer();
	
	vk::Image get_image() const;
	vk::ImageView get_image_view() const;
	vk::Extent2D get_extent() const;
};
