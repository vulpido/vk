#pragma once

#include <optional>
#include <string>
#include <memory>

#include <vulkan/vulkan.hpp>

class Application;
class Window;

using vk_UniqueDynamicDebugUtilsMessenger = vk::UniqueHandle<vk::DebugUtilsMessengerEXT, vk::DispatchLoaderDynamic>;

struct SwapchainSupport {
	vk::SurfaceCapabilitiesKHR capabilities;
	std::vector<vk::SurfaceFormatKHR> formats;
	std::vector<vk::PresentModeKHR> present_modes;
	
	SwapchainSupport(vk::PhysicalDevice device, vk::SurfaceKHR surface);
};

class Renderer {
private:
	const Application &application;
	
	vk::UniqueInstance instance;
	
	vk::DispatchLoaderDynamic dynamic_loader;
	vk_UniqueDynamicDebugUtilsMessenger messenger;
	
	vk::UniqueSurfaceKHR surface;
	vk::PhysicalDevice physical_device;
	vk::UniqueDevice device;
	
	uint32_t graphics_queue_index;
	uint32_t present_queue_index;
	vk::Queue graphics_queue;
	vk::Queue present_queue;
	
	void create_instance(const std::string &name, uint32_t major, uint32_t minor, uint32_t revision);
	void setup_debug_messenger();
	void create_surface(std::shared_ptr<Window>);
	void create_logical_device();
	
protected:
	virtual bool use_validation_layers() const;
	virtual std::vector<const char*> get_validation_layers() const;
	
	virtual std::vector<const char*> get_instance_extensions() const;
	virtual std::vector<const char*> get_device_extensions() const;
	
	/// Indicates whether a given vk::PhysicalDevice is suitable for the renderer
	virtual bool is_device_suitable(vk::PhysicalDevice);
	
	/// Select first suitable device
	virtual void pick_physical_device();
	
public:
	Renderer(const Application&, std::shared_ptr<Window>);
	virtual ~Renderer();
	
	uint32_t find_memory_type(uint32_t type_filter, vk::MemoryPropertyFlags) const;
	
	virtual vk::SurfaceFormatKHR choose_swapchain_format(const std::vector<vk::SurfaceFormatKHR>&) const;
	virtual vk::PresentModeKHR choose_swapchain_present_mode(const std::vector<vk::PresentModeKHR>&) const;
	
	vk::Instance get_instance() const;
	vk::PhysicalDevice get_physical_device() const;
	vk::Device get_device() const;
};
