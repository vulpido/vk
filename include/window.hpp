#pragma once

#include <string>

#if defined(__linux__)
#elif defined(__WIN32)
#include "window_win32.hpp"
#endif

class Application;

#if defined(__linux__)
class Window : public BaseWindow {

#elif defined(__WIN32)
class Window : public BaseWindow<Window> {

#endif

protected:
	const Application &application;

	bool should_close = false;

public:
	Window(
		const Application&,
		const std::string &title,
		uint32_t width,
		uint32_t height
	);

	~Window();

	void show();
	void hide();

	void set_visible(bool);
	bool get_visible() const;

	void set_title(const std::string&);
	std::string get_title() const;

	uint32_t get_width() const;
	uint32_t get_height() const;

	void handle_events();

	bool get_should_close() const;
};
