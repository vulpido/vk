#pragma once

#include <xcb/xcb.h>

class BaseWindow {
protected:
	xcb_connection_t *connection;
	xcb_window_t handle;

	xcb_intern_atom_reply_t *reply_window_delete;

public:
	xcb_connection_t* get_connection() const;
	xcb_window_t get_handle() const;
}
